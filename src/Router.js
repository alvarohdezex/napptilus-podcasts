import React from "react";
import { Route, Switch } from "react-router-dom";
import { Podcasts, NotFound, Podcast } from "./containers";

const AppRouter = () => (
  <Switch>
    <Route exact path="/" component={Podcasts} />
    <Route exact path="/podcasts" component={Podcasts} />
    <Route exact path="/podcast/:id" component={Podcast} />
    <Route exact path="/podcast/:id/episode/:guid" component={Podcast} />
    <Route component={NotFound} />
  </Switch>
);

export default AppRouter;
