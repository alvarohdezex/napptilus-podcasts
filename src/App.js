import React from "react";
import styled from "@emotion/styled";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";

import AppRouter from "./Router";
import { store } from "./redux";
import { Header } from "./components";

const StyledContainer = styled.div`
  display: flex;
  width: 65vw;
  height: 100vh;
  flex-direction: column;
  font-family: Arial, Helvetica, sans-serif;
  margin: auto;
`;

function App() {
  return (
    <Provider store={store}>
      <Router>
        <StyledContainer>
          <Header />
          <AppRouter />
        </StyledContainer>
      </Router>
    </Provider>
  );
}

export default App;
