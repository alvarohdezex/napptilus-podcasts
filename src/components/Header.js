import React from "react";
import styled from "@emotion/styled";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "react-loader-spinner";

const StyledHeader = styled.div`
  display: flex;
  width: 100%;
  border-bottom: 1px solid #dfe6e9;
  padding-bottom: 5px;
  margin-bottom: 20px;
  align-items: center;
  justify-content: space-between;
  height: 40px;
`;

const Title = styled(Link)`
  text-decoration: none;
  color: #0984e3;
  font-weight: bold;
`;

function Header({ loading }) {
  return (
    <StyledHeader>
      <Title to="/">Podcaster</Title>
      <Loader
        visible={loading}
        width={25}
        height={25}
        type="TailSpin"
        color="#0984e3"
      />
    </StyledHeader>
  );
}

const mapStateToProps = ({ podcasts }) => {
  const { loading } = podcasts;
  return { loading };
};

export default connect(mapStateToProps)(Header);
