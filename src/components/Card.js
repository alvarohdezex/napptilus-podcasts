import React from "react";
import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  width: 250px;
  margin-bottom: 50px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
  justify-content: flex-end;
  align-items: center;
  height: 110px;
  flex-direction: column;
  padding-bottom: 5px;
  padding-left: 5px;
  padding-right: 5px;
  cursor: pointer;
`;

const Image = styled.img`
  height: 80px;
  width: 80px;
  border-radius: 40px;
  position: absolute;
  transform: translateY(-65px);
`;

const Title = styled.span`
  font-size: 12px;
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
`;

const Author = styled.span`
  font-size: 10px;
  font-weight: bold;
  text-align: center;
  color: #b2bec3;
  margin-top: 10px;
`;

function Card({ artist, title, images }) {
  return (
    <Container>
      <Image src={images[2].src} />
      <Title>{title}</Title>
      <Author>Author: {artist}</Author>
    </Container>
  );
}

export default Card;
