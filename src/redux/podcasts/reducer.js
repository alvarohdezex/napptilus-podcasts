import { podcastsActions } from "./actions";

const initialState = {
  podcastList: [],
  podcast: {},
  loading: true,
  error: "Error",
};

export default function(state = initialState, { type, payload }) {
  const { podcastListClone } = state;

  switch (type) {
    case podcastsActions.GET_PODCASTS_REQUEST:
      return { ...state, loading: true, error: null };

    case podcastsActions.GET_PODCASTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        podcastList: [...payload],
        podcastListClone: [...payload],
      };

    case podcastsActions.GET_PODCASTS_FAILURE:
      return { ...state, loading: false, error: payload };

    case podcastsActions.FILTER_PODCASTS:
      const podcastsFiltered = podcastListClone;
      return {
        ...state,
        podcastList: podcastsFiltered.filter(
          p =>
            p.artist.toLowerCase().indexOf(payload.toLowerCase()) !== -1 ||
            p.title.toLowerCase().indexOf(payload.toLowerCase()) !== -1
        ),
      };

    case podcastsActions.GET_PODCAST_REQUEST:
      return { ...state, loading: true, error: null };

    case podcastsActions.GET_PODCAST_SUCCESS:
      return { ...state, loading: false, error: null, podcast: { ...payload } };

    case podcastsActions.GET_PODCAST_FAILURE:
      return { ...state, loading: false, error: payload };

    default:
      return { ...state };
  }
}
