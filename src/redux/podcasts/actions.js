import api from "../../utils/api";

export const podcastsActions = {
  GET_PODCASTS_REQUEST: "GET_PODCASTS_REQUEST",
  GET_PODCASTS_SUCCESS: "GET_PODCASTS_SUCCESS",
  GET_PODCASTS_FAILURE: "GET_PODCASTS_FAILURE",
  FILTER_PODCASTS: "FILTER_PODCASTS",
  GET_PODCAST_REQUEST: "GET_PODCAST_REQUEST",
  GET_PODCAST_SUCCESS: "GET_PODCAST_SUCCESS",
  GET_PODCAST_FAILURE: "GET_PODCAST_FAILURE",
};

export const getPodcastsRequest = () => {
  return dispatch => {
    dispatch(
      { type: podcastsActions.GET_PODCASTS_REQUEST },
      getPodcasts(dispatch)
    );
  };
};

const getPodcasts = async dispatch => {
  try {
    let podcasts = [];
    let dayHasPassed;
    const now = new Date().getTime();
    const day = 1000 * 60 * 60 * 24;
    const { podcastsStorage, podcastsDate } = localStorage;

    if (podcastsDate) {
      dayHasPassed = now - parseInt(podcastsDate) > day;
    }

    if (podcastsStorage && !dayHasPassed) {
      podcasts = await JSON.parse(podcastsStorage);
      dispatch({
        type: podcastsActions.GET_PODCASTS_SUCCESS,
        payload: podcasts,
      });
    } else {
      const result = await api.get(
        `https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json`
      );

      if (result && result.data && result.data.feed && result.data.feed.entry) {
        podcasts = [...result.data.feed.entry];
      }

      localStorage.setItem("podcastsDate", new Date().getTime());
      localStorage.setItem(
        "podcastsStorage",
        JSON.stringify(transformPodcasts(podcasts))
      );

      dispatch({
        type: podcastsActions.GET_PODCASTS_SUCCESS,
        payload: transformPodcasts(podcasts),
      });
    }
  } catch (e) {
    console.log("Error fetching podcasts", e);
    dispatch({
      type: podcastsActions.GET_PODCASTS_FAILURE,
      payload: e && e.response && e.response.data && e.response.data.message,
    });
  }
};

export const transformPodcasts = (podcasts = []) => {
  return podcasts.map(p => {
    const category = p.category.attributes.term;
    const id = p.id.attributes["im:id"];
    const artist = p["im:artist"].label;
    const contentType = p["im:contentType"].attributes.term;
    const images = [
      ...p["im:image"].map(i => ({
        src: i.label,
        height: i.attributes.height,
      })),
    ];
    const name = p["im:name"].label;
    const price = p["im:price"].attributes.amount;
    const releaseDate = p["im:releaseDate"].attributes.label;
    const link = p.link.attributes.href;
    const rights = p.rights ? p.rights.label : "";
    const summary = p.summary ? p.summary.label : "";
    const title = p.title ? p.title.label : "";

    return {
      category,
      id,
      artist,
      contentType,
      images,
      name,
      price,
      releaseDate,
      link,
      rights,
      summary,
      title,
    };
  });
};

export const filterPodcasts = pattern => ({
  type: podcastsActions.FILTER_PODCASTS,
  payload: pattern,
});

export const getPodcastRequest = id => {
  return dispatch => {
    dispatch(
      { type: podcastsActions.GET_PODCAST_REQUEST },
      getPodcast(id, dispatch)
    );
  };
};

const getPodcast = async (id, dispatch) => {
  try {
    let podcast = {};
    let dayHasPassed;
    const now = new Date().getTime();
    const day = 1000 * 60 * 60 * 24;

    const podcastStorage = localStorage.getItem(`podcast_${id}`);
    const podcastDate = localStorage.getItem(`podcast_${id}_date`);

    if (podcastDate) {
      dayHasPassed = now - parseInt(podcastDate) > day;
    }

    if (podcastStorage && !dayHasPassed) {
      podcast = await JSON.parse(podcastStorage);
      dispatch({
        type: podcastsActions.GET_PODCAST_SUCCESS,
        payload: {
          ...podcast,
        },
      });
    } else {
      const result = await api.get(
        `https://cors-anywhere.herokuapp.com/https://itunes.apple.com/lookup?id=${id}`
      );
      podcast = { ...result.data.results[0] };
      const episodes = await api.get(
        `https://api.rss2json.com/v1/api.json?rss_url=${podcast.feedUrl}`
      );
      podcast = {
        ...podcast,
        episodes: episodes.data.items ? episodes.data.items : [],
      };

      localStorage.setItem(`podcast_${id}`, JSON.stringify(podcast));
      localStorage.setItem(`podcast_${id}_date`, new Date().getTime());
      dispatch({
        type: podcastsActions.GET_PODCAST_SUCCESS,
        payload: {
          ...podcast,
        },
      });
    }
  } catch (e) {
    console.log("Error fetching the podcast", e);
    dispatch({
      type: podcastsActions.GET_PODCAST_FAILURE,
      payload: e && e.response && e.response.data && e.response.data.message,
    });
  }
};
