import { combineReducers } from "redux";
import podcastsReducer from "./podcasts/reducer";

export default combineReducers({
  podcasts: podcastsReducer,
});
