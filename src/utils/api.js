import axios from "axios";

const get = url => {
  return axios.get(url);
};

const api = {
  get,
};

export default api;
