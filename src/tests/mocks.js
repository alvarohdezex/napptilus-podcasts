export const podcastsMock = [
  {
    category: "Music History",
    id: "1481398762",
    artist: "WNYC Studios & OSM Audio",
    contentType: "Podcast",
    images: [
      {
        src:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/55x55bb.png",
        height: "55",
      },
      {
        src:
          "https://is2-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/60x60bb.png",
        height: "60",
      },
      {
        src:
          "https://is2-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/170x170bb.png",
        height: "170",
      },
    ],
    name: "Dolly Parton's America",
    price: "0",
    releaseDate: "October 15, 2019",
    link:
      "https://podcasts.apple.com/us/podcast/dolly-partons-america/id1481398762?uo=2",
    rights: "© WNYC Studios & OSM Audio",
    summary:
      "In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. Join us for a 9-episode journey into the Dollyverse. Hosted by Jad Abumrad, creator of Radiolab and More Perfect.\nDolly Parton’s America is co-produced by OSM Audio and WNYC Studios.",
    title: "Dolly Parton's America - WNYC Studios & OSM Audio",
  },
  {
    category: "Music Interviews",
    id: "472939437",
    artist: "WNYC Studios",
    contentType: "Podcast",
    images: [
      {
        src:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/55x55bb.png",
        height: "55",
      },
      {
        src:
          "https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/60x60bb.png",
        height: "60",
      },
      {
        src:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/170x170bb.png",
        height: "170",
      },
    ],
    name: "Here's The Thing with Alec Baldwin",
    price: "0",
    releaseDate: "October 15, 2019",
    link:
      "https://podcasts.apple.com/us/podcast/heres-the-thing-with-alec-baldwin/id472939437?uo=2",
    rights: "© WNYC",
    summary:
      "From WNYC Studios, award-winning actor Alec Baldwin takes listeners into the lives of artists, policy makers and performers.  Alec sidesteps the predictable by going inside the dressing rooms, apartments, and offices of people we want to understand better:  Ira Glass, Lena Dunham, David Brooks, Roz Chast, Chris Rock and others. Hear what happens when an inveterate guest becomes a host.\nWNYC Studios is a listener-supported producer of other leading podcasts including Radiolab, Snap Judgment, On the Media, Death, Sex & Money, Nancy and many others.   \n © WNYC Studios",
    title: "Here's The Thing with Alec Baldwin - WNYC Studios",
  },
];

export const podcastsWithoutTransform = [
  {
    "im:name": {
      label: "Dolly Parton's America",
    },
    "im:image": [
      {
        label:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/55x55bb.png",
        attributes: {
          height: "55",
        },
      },
      {
        label:
          "https://is2-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/60x60bb.png",
        attributes: {
          height: "60",
        },
      },
      {
        label:
          "https://is2-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/170x170bb.png",
        attributes: {
          height: "170",
        },
      },
    ],
    summary: {
      label:
        "In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. Join us for a 9-episode journey into the Dollyverse. Hosted by Jad Abumrad, creator of Radiolab and More Perfect.\nDolly Parton’s America is co-produced by OSM Audio and WNYC Studios.",
    },
    "im:price": {
      label: "Get",
      attributes: {
        amount: "0",
        currency: "USD",
      },
    },
    "im:contentType": {
      attributes: {
        term: "Podcast",
        label: "Podcast",
      },
    },
    rights: {
      label: "© WNYC Studios & OSM Audio",
    },
    title: {
      label: "Dolly Parton's America - WNYC Studios & OSM Audio",
    },
    link: {
      attributes: {
        rel: "alternate",
        type: "text/html",
        href:
          "https://podcasts.apple.com/us/podcast/dolly-partons-america/id1481398762?uo=2",
      },
    },
    id: {
      label:
        "https://podcasts.apple.com/us/podcast/dolly-partons-america/id1481398762?uo=2",
      attributes: {
        "im:id": "1481398762",
      },
    },
    "im:artist": {
      label: "WNYC Studios & OSM Audio",
    },
    category: {
      attributes: {
        "im:id": "1524",
        term: "Music History",
        scheme:
          "https://podcasts.apple.com/us/genre/podcasts-music-music-history/id1524?uo=2",
        label: "Music History",
      },
    },
    "im:releaseDate": {
      label: "2019-10-15T09:00:00-07:00",
      attributes: {
        label: "October 15, 2019",
      },
    },
  },
  {
    "im:name": {
      label: "Here's The Thing with Alec Baldwin",
    },
    "im:image": [
      {
        label:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/55x55bb.png",
        attributes: {
          height: "55",
        },
      },
      {
        label:
          "https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/60x60bb.png",
        attributes: {
          height: "60",
        },
      },
      {
        label:
          "https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/1a/00/7f1a0029-317d-ca2b-5d34-b904bca2aab6/mza_3720994942612065093.jpg/170x170bb.png",
        attributes: {
          height: "170",
        },
      },
    ],
    summary: {
      label:
        "From WNYC Studios, award-winning actor Alec Baldwin takes listeners into the lives of artists, policy makers and performers.  Alec sidesteps the predictable by going inside the dressing rooms, apartments, and offices of people we want to understand better:  Ira Glass, Lena Dunham, David Brooks, Roz Chast, Chris Rock and others. Hear what happens when an inveterate guest becomes a host.\nWNYC Studios is a listener-supported producer of other leading podcasts including Radiolab, Snap Judgment, On the Media, Death, Sex & Money, Nancy and many others.   \n © WNYC Studios",
    },
    "im:price": {
      label: "Get",
      attributes: {
        amount: "0",
        currency: "USD",
      },
    },
    "im:contentType": {
      attributes: {
        term: "Podcast",
        label: "Podcast",
      },
    },
    rights: {
      label: "© WNYC",
    },
    title: {
      label: "Here's The Thing with Alec Baldwin - WNYC Studios",
    },
    link: {
      attributes: {
        rel: "alternate",
        type: "text/html",
        href:
          "https://podcasts.apple.com/us/podcast/heres-the-thing-with-alec-baldwin/id472939437?uo=2",
      },
    },
    id: {
      label:
        "https://podcasts.apple.com/us/podcast/heres-the-thing-with-alec-baldwin/id472939437?uo=2",
      attributes: {
        "im:id": "472939437",
      },
    },
    "im:artist": {
      label: "WNYC Studios",
      attributes: {
        href: "https://podcasts.apple.com/us/artist/wnyc/127981066?uo=2",
      },
    },
    category: {
      attributes: {
        "im:id": "1525",
        term: "Music Interviews",
        scheme:
          "https://podcasts.apple.com/us/genre/podcasts-music-music-interviews/id1525?uo=2",
        label: "Music Interviews",
      },
    },
    "im:releaseDate": {
      label: "2019-10-15T09:00:00-07:00",
      attributes: {
        label: "October 15, 2019",
      },
    },
  },
];

export const podcastMock = {
  wrapperType: "track",
  kind: "podcast",
  collectionId: 1481398762,
  trackId: 1481398762,
  artistName: "WNYC Studios & OSM Audio",
  collectionName: "Dolly Parton's America",
  trackName: "Dolly Parton's America",
  collectionCensoredName: "Dolly Parton's America",
  trackCensoredName: "Dolly Parton's America",
  collectionViewUrl:
    "https://podcasts.apple.com/us/podcast/dolly-partons-america/id1481398762?uo=4",
  feedUrl: "http://feeds.wnyc.org/dolly-partons-america",
  trackViewUrl:
    "https://podcasts.apple.com/us/podcast/dolly-partons-america/id1481398762?uo=4",
  artworkUrl30:
    "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/30x30bb.jpg",
  artworkUrl60:
    "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/60x60bb.jpg",
  artworkUrl100:
    "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/100x100bb.jpg",
  collectionPrice: 0,
  trackPrice: 0,
  trackRentalPrice: 0,
  collectionHdPrice: 0,
  trackHdPrice: 0,
  trackHdRentalPrice: 0,
  releaseDate: "2019-10-15T16:00:00Z",
  collectionExplicitness: "cleaned",
  trackExplicitness: "cleaned",
  trackCount: 2,
  country: "USA",
  currency: "USD",
  primaryGenreName: "Music History",
  contentAdvisoryRating: "Clean",
  artworkUrl600:
    "https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/19/4b/85/194b85a6-a150-63cf-5e0e-da3c354f1096/mza_8312636282593699004.jpg/600x600bb.jpg",
  genreIds: ["1524", "26", "1310", "1324", "1543"],
  genres: [
    "Music History",
    "Podcasts",
    "Music",
    "Society & Culture",
    "Documentary",
  ],
  episodes: [
    {
      title: "Sad Ass Songs",
      pubDate: "2019-10-15 16:00:00",
      link: "http://www.wnycstudios.org/story/sad-ass-songs/",
      guid: "f95e7f13-9f71-4e44-9266-e8616ca1f2e1",
      author: "wnycdigital@gmail.com (WNYC Studios &amp; OSM Audio )",
      thumbnail:
        "https://media.wnyc.org/i/130/130/l/80/2019/10/Sad-Ass-Songs-DollyParton.png",
      description:
        "\n<p><span>We begin with a simple question: How did the queen of the boob joke become a feminist icon? Helen Morales, author of “Pilgrimage to Dollywood,” gave us a stern directive – look at the lyrics! </span><span>So we dive into </span><span>Dolly’s discography, starting with the early period of what Dolly calls “sad ass songs” to find remarkably prescient words of female pain, slut-shaming, domestic violence, and women being locked away in asylums by cheating husbands. We explore how Dolly took the centuries-old tradition of the Appalachian “murder ballad”—an oral tradition of men singing songs about brutally killing women—and flipped the script, singing from the woman’s point of view. And as her career progresses, the songs expand beyond the pain to tell tales of leaving abuse behind.</span></p>\n<p><span>How can such pro-woman lyrics come from someone who despises the word feminism? Dolly explains.  </span></p>\n",
      content:
        "\n<p>We begin with a simple question: How did the queen of the boob joke become a feminist icon? Helen Morales, author of “Pilgrimage to Dollywood,” gave us a stern directive – look at the lyrics! So we dive into Dolly’s discography, starting with the early period of what Dolly calls “sad ass songs” to find remarkably prescient words of female pain, slut-shaming, domestic violence, and women being locked away in asylums by cheating husbands. We explore how Dolly took the centuries-old tradition of the Appalachian “murder ballad”—an oral tradition of men singing songs about brutally killing women—and flipped the script, singing from the woman’s point of view. And as her career progresses, the songs expand beyond the pain to tell tales of leaving abuse behind.</p>\n<p>How can such pro-woman lyrics come from someone who despises the word feminism? Dolly explains.  </p>\n",
      enclosure: {
        link:
          "https://www.podtrac.com/pts/redirect.mp3/audio.wnyc.org/dpa/dpa101519_sadasssongs.mp3?aw_0_awz.podcast_clash_enabled=1",
        type: "audio/mpeg",
        duration: 3432,
        thumbnail:
          "https://media.wnyc.org/i/130/130/l/80/2019/10/Sad-Ass-Songs-DollyParton.png",
        rating: { scheme: "urn:itunes", value: "no" },
      },
      categories: ["documentary", "music", "storytelling"],
    },
    {
      title: "Dolly Parton's America Trailer",
      pubDate: "2019-10-03 16:00:00",
      link: "http://www.wnycstudios.org/story/dolly-partons-america-trailer/",
      guid: "328a798c-d0ff-4a09-a275-278cd3a1c678",
      author: "wnycdigital@gmail.com (WNYC Studios &amp; OSM Audio )",
      thumbnail:
        "https://media.wnyc.org/i/130/130/l/80/2019/09/dolly-parton-america-trailer-image.png",
      description:
        "<p><span>In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. </span></p>",
      content:
        "\n<p><span>In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. Join us for a 9-part journey into the Dollyverse. </span></p>\n<p><span>Hosted by Jad Abumrad, creator of </span><i><span>Radiolab</span></i><span> and </span><i><span>More Perfect</span></i><span>.</span></p>\n",
      enclosure: {
        link:
          "https://www.podtrac.com/pts/redirect.mp3/audio.wnyc.org/dpa/dpa100319_trailer.mp3?aw_0_awz.podcast_clash_enabled=1",
        type: "audio/mpeg",
        duration: 64,
        thumbnail:
          "https://media.wnyc.org/i/130/130/l/80/2019/09/dolly-parton-america-trailer-image.png",
        rating: { scheme: "urn:itunes", value: "no" },
      },
      categories: ["life", "music", "storytelling"],
    },
  ],
};

export const episodesMock = [
  {
    title: "Sad Ass Songs",
    pubDate: "2019-10-15",
    link: "http://www.wnycstudios.org/story/sad-ass-songs/",
    guid: "f95e7f13-9f71-4e44-9266-e8616ca1f2e1",
    author: "wnycdigital@gmail.com (WNYC Studios &amp; OSM Audio )",
    thumbnail:
      "https://media.wnyc.org/i/130/130/l/80/2019/10/Sad-Ass-Songs-DollyParton.png",
    description:
      "\n<p><span>We begin with a simple question: How did the queen of the boob joke become a feminist icon? Helen Morales, author of “Pilgrimage to Dollywood,” gave us a stern directive – look at the lyrics! </span><span>So we dive into </span><span>Dolly’s discography, starting with the early period of what Dolly calls “sad ass songs” to find remarkably prescient words of female pain, slut-shaming, domestic violence, and women being locked away in asylums by cheating husbands. We explore how Dolly took the centuries-old tradition of the Appalachian “murder ballad”—an oral tradition of men singing songs about brutally killing women—and flipped the script, singing from the woman’s point of view. And as her career progresses, the songs expand beyond the pain to tell tales of leaving abuse behind.</span></p>\n<p><span>How can such pro-woman lyrics come from someone who despises the word feminism? Dolly explains.  </span></p>\n",
    content:
      "\n<p>We begin with a simple question: How did the queen of the boob joke become a feminist icon? Helen Morales, author of “Pilgrimage to Dollywood,” gave us a stern directive – look at the lyrics! So we dive into Dolly’s discography, starting with the early period of what Dolly calls “sad ass songs” to find remarkably prescient words of female pain, slut-shaming, domestic violence, and women being locked away in asylums by cheating husbands. We explore how Dolly took the centuries-old tradition of the Appalachian “murder ballad”—an oral tradition of men singing songs about brutally killing women—and flipped the script, singing from the woman’s point of view. And as her career progresses, the songs expand beyond the pain to tell tales of leaving abuse behind.</p>\n<p>How can such pro-woman lyrics come from someone who despises the word feminism? Dolly explains.  </p>\n",
    enclosure: {
      link:
        "https://www.podtrac.com/pts/redirect.mp3/audio.wnyc.org/dpa/dpa101519_sadasssongs.mp3?aw_0_awz.podcast_clash_enabled=1",
      type: "audio/mpeg",
      duration: 3432,
      thumbnail:
        "https://media.wnyc.org/i/130/130/l/80/2019/10/Sad-Ass-Songs-DollyParton.png",
      rating: { scheme: "urn:itunes", value: "no" },
    },
    categories: ["documentary", "music", "storytelling"],
  },
  {
    title: "Dolly Parton's America Trailer",
    pubDate: "2019-10-03",
    link: "http://www.wnycstudios.org/story/dolly-partons-america-trailer/",
    guid: "328a798c-d0ff-4a09-a275-278cd3a1c678",
    author: "wnycdigital@gmail.com (WNYC Studios &amp; OSM Audio )",
    thumbnail:
      "https://media.wnyc.org/i/130/130/l/80/2019/09/dolly-parton-america-trailer-image.png",
    description:
      "<p><span>In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. </span></p>",
    content:
      "\n<p><span>In this intensely divided moment, one of the few things everyone still seems to agree on is Dolly Parton—but why? That simple question leads to a deeply personal, historical, and musical rethinking of one of America’s great icons. Join us for a 9-part journey into the Dollyverse. </span></p>\n<p><span>Hosted by Jad Abumrad, creator of </span><i><span>Radiolab</span></i><span> and </span><i><span>More Perfect</span></i><span>.</span></p>\n",
    enclosure: {
      link:
        "https://www.podtrac.com/pts/redirect.mp3/audio.wnyc.org/dpa/dpa100319_trailer.mp3?aw_0_awz.podcast_clash_enabled=1",
      type: "audio/mpeg",
      duration: 64,
      thumbnail:
        "https://media.wnyc.org/i/130/130/l/80/2019/09/dolly-parton-america-trailer-image.png",
      rating: { scheme: "urn:itunes", value: "no" },
    },
    categories: ["life", "music", "storytelling"],
  },
];
