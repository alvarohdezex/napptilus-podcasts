import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render } from "@testing-library/react";
import { Podcast } from "../containers/Podcast";
import { getPodcastRequest } from "../redux/podcasts/actions";
import { podcastMock, podcastsMock } from "./mocks";

jest.mock("../redux/podcasts/actions", () => ({
  getPodcastRequest: jest.fn(),
}));

describe("<Podcast />", () => {
  const component = render(
    <Router>
      <Podcast getPodcastRequest={getPodcastRequest} podcast={podcastMock} />
    </Router>
  );

  it("Should match snapshot", () => {
    const { container } = component;
    expect(container).toMatchSnapshot();
  });

  it("Should render a 'loading...' if loading prop is true", async () => {
    const { findByText } = render(
      <Router>
        <Podcast
          getPodcastRequest={getPodcastRequest}
          podcast={{}}
          loading={true}
        />
      </Router>
    );
    const message = await findByText("Loading...");
    expect(message.textContent).toBe("Loading...");
  });

  it("Should call getPodcastsRequest", () => {
    expect(getPodcastRequest).toHaveBeenCalled();
  });

  it("Should have the name of the podcast", async () => {
    const { findByText } = render(
      <Router>
        <Podcast
          getPodcastRequest={getPodcastRequest}
          podcast={podcastMock}
          loading={false}
        />
      </Router>
    );
    const name = await findByText("Dolly Parton's America");
    expect(name.textContent).toBe(podcastMock.trackName);
  });

  it("Should have the description of the podcast", async () => {
    // I mocked the storage for Jest on setupTestFramework
    // I retrieve the podcast from the storage instead of making an api call
    localStorage.setItem("podcastsStorage", JSON.stringify(podcastsMock));

    const { findByTestId } = render(
      <Router>
        <Podcast
          getPodcastRequest={getPodcastRequest}
          podcast={podcastMock}
          loading={false}
        />
      </Router>
    );

    const description = await findByTestId("podcast-description");
    expect(description.textContent).toBe(podcastsMock[0].summary);
  });
});
