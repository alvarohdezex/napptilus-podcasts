import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render } from "@testing-library/react";
import { Episode } from "../containers";
import { episodesMock } from "./mocks";

describe("<Episode />", () => {
  const component = render(
    <Router>
      <Episode episode={episodesMock[0]} />
    </Router>
  );

  it("Should match the snapshot", () => {
    const { container } = component;
    expect(container).toMatchSnapshot();
  });

  it("Should have the title of the episode", () => {
    const { getByTestId } = render(
      <Router>
        <Episode episode={episodesMock[0]} />
      </Router>
    );

    const title = getByTestId("episode-title");
    expect(title.textContent).toBe(episodesMock[0].title);
  });

  it("Should have the description of the episode", () => {
    const { getByTestId } = render(
      <Router>
        <Episode episode={episodesMock[0]} />
      </Router>
    );

    const description = getByTestId("episode-description");
    expect(description.textContent).toBe(
      episodesMock[0].description.replace(/<[^>]+>/g, "")
    );
  });

  it("Should have the audio of the episode", () => {
    const { getByTestId } = render(
      <Router>
        <Episode episode={episodesMock[0]} />
      </Router>
    );

    const audio = getByTestId("episode-audio");
    expect(audio.children.item(0).hasAttribute("src")).toBe(true);
  });
});
