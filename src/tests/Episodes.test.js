import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render } from "@testing-library/react";
import { Episodes } from "../containers";
import { episodesMock } from "./mocks";
import { fancyTimeFormat } from "../utils/dates";

describe("<Episodes />", () => {
  const component = render(
    <Router>
      <Episodes episodes={episodesMock} />
    </Router>
  );

  it("Should match the snapshot", () => {
    const { container } = component;
    expect(container).toMatchSnapshot();
  });

  it("Should display a count with the episodes number", async () => {
    const { findByTestId } = render(
      <Router>
        <Episodes episodes={episodesMock} />
      </Router>
    );

    const count = await findByTestId("episodes-count");
    expect(count.textContent).toBe("Episodes: 2");
  });

  it("Should display a table with 2 episodes", async () => {
    const { findAllByTestId } = render(
      <Router>
        <Episodes episodes={episodesMock} />
      </Router>
    );

    const episodesNumber = await findAllByTestId("episode");
    expect(episodesNumber.length).toBe(2);
  });

  it("Episodes should have a title", async () => {
    const { findAllByTestId } = render(
      <Router>
        <Episodes episodes={episodesMock} />
      </Router>
    );

    const episodeTitle = await findAllByTestId("episode-title");
    expect(episodeTitle[0].textContent).toBe(episodesMock[0].title);
  });

  it("Episodes should have a date", async () => {
    const { findAllByTestId } = render(
      <Router>
        <Episodes episodes={episodesMock} />
      </Router>
    );

    const episodeDate = await findAllByTestId("episode-date");
    expect(episodeDate[0].textContent).toBe(episodesMock[0].pubDate);
  });

  it("Episodes should have a duration", async () => {
    const { findAllByTestId } = render(
      <Router>
        <Episodes episodes={episodesMock} />
      </Router>
    );

    const episodeDuration = await findAllByTestId("episode-duration");
    expect(episodeDuration[0].textContent).toBe(
      fancyTimeFormat(episodesMock[0].enclosure.duration)
    );
  });
});
