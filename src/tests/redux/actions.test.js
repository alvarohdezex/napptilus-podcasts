import { podcastsMock, podcastsWithoutTransform } from "../mocks";
import {
  transformPodcasts,
  podcastsActions,
  filterPodcasts,
} from "../../redux/podcasts/actions";

describe("Redux actions", () => {
  it("Transform podcasts should receive a list of podcast and transform them", () => {
    expect(podcastsMock).toStrictEqual(
      transformPodcasts(podcastsWithoutTransform)
    );
  });

  it("Dispatch FILTER_PODCASTS when filterPodcasts is called", () => {
    const expectedAction = {
      type: podcastsActions.FILTER_PODCASTS,
      payload: "Napptilus",
    };

    expect(filterPodcasts("Napptilus")).toStrictEqual(expectedAction);
  });
});
