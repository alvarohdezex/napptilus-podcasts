import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render, fireEvent } from "@testing-library/react";
import { Podcasts } from "../containers/Podcasts";
import { getPodcastsRequest, filterPodcasts } from "../redux/podcasts/actions";

import { podcastsMock } from "./mocks";

jest.mock("../redux/podcasts/actions", () => ({
  getPodcastsRequest: jest.fn(),
  filterPodcasts: jest.fn(),
}));

describe("<Podcasts />", () => {
  const component = render(
    <Router>
      <Podcasts
        getPodcastsRequest={getPodcastsRequest}
        podcastList={podcastsMock}
      />
    </Router>
  );

  it("Should match snapshot", () => {
    const { container } = component;
    expect(container).toMatchSnapshot();
  });

  it("Should render a 'loading...' if loading prop is true", async () => {
    const { findByText } = render(
      <Podcasts getPodcastsRequest={getPodcastsRequest} loading={true} />
    );
    const message = await findByText("Loading...");
    expect(message.textContent).toBe("Loading...");
  });

  it("Should call getPodcastsRequest", () => {
    expect(getPodcastsRequest).toHaveBeenCalled();
  });

  it("Should show a count with the podcasts number, 2", async () => {
    const { getByTestId } = render(
      <Router>
        <Podcasts
          getPodcastsRequest={getPodcastsRequest}
          podcastList={podcastsMock}
        />
      </Router>
    );

    const count = getByTestId("podcast-count");
    expect(count.textContent).toBe("2");
  });

  it("Should really have 2 podcasts", async () => {
    const { findAllByTestId } = render(
      <Router>
        <Podcasts
          getPodcastsRequest={getPodcastsRequest}
          podcastList={podcastsMock}
        />
      </Router>
    );

    const children = await findAllByTestId("podcast");
    expect(children.length).toBe(2);
  });

  it("Should call filter podcasts if we type in the filter", () => {
    const { getByTestId } = render(
      <Router>
        <Podcasts
          filterPodcasts={filterPodcasts}
          getPodcastsRequest={getPodcastsRequest}
          podcastList={podcastsMock}
        />
      </Router>
    );

    const filter = getByTestId("podcast-filter");
    fireEvent.change(filter, { target: { value: "Napptilus" } });
    expect(filterPodcasts).toHaveBeenCalled();
  });
});
