import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import styled from "@emotion/styled";

import { Card } from "../components";
import { getPodcastsRequest, filterPodcasts } from "../redux/podcasts/actions";

const Container = styled.div`
  display: flex;
  flex-direction: column;

  a {
    text-decoration: none;
    color: unset !important;
  }
`;

const PodcastContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 60px;
  align-self: flex-end;
`;

const PodcastCount = styled.div`
  background: #0984e3;
  height: 20px;
  width: 35px;
  margin-right: 15px;
  text-align: center;
  border-radius: 6px;
  color: white;
  font-weight: bold;
`;

const Filter = styled.input`
  padding-left: 5px;
  height: 22px;
  border-radius: 5px;
  border: 1px solid #dfe6e9;
`;

export function Podcasts({
  getPodcastsRequest,
  filterPodcasts,
  podcastList,
  loading,
  error,
}) {
  const [pattern, setPattern] = useState("");

  useEffect(() => {
    getPodcastsRequest();
  }, [getPodcastsRequest]);

  const handleOnChange = event => {
    const pattern = event.target.value;
    setPattern(pattern);
    filterPodcasts(pattern);
  };

  return (
    <Container>
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <>
          <Row>
            <PodcastCount data-testid="podcast-count">
              {podcastList.length}
            </PodcastCount>
            <Filter
              data-testid="podcast-filter"
              placeholder="Filter podcasts..."
              onChange={handleOnChange}
              value={pattern}
            />
          </Row>
          <PodcastContainer>
            {podcastList.map(podcast => (
              <Link
                key={podcast.id}
                to={`/podcast/${podcast.id}`}
                data-testid="podcast"
              >
                <Card {...podcast} />
              </Link>
            ))}
          </PodcastContainer>
        </>
      )}
    </Container>
  );
}

const mapStateToProps = ({ podcasts }) => {
  const { podcastList, loading, error } = podcasts;
  return { podcastList, loading, error };
};

export default connect(
  mapStateToProps,
  { getPodcastsRequest, filterPodcasts }
)(Podcasts);
