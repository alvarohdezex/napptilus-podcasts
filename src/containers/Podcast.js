import React, { useEffect } from "react";
import styled from "@emotion/styled";
import { connect } from "react-redux";
import { useParams, Route, Link } from "react-router-dom";
import { getPodcastRequest } from "../redux/podcasts/actions";

import { Episodes, Episode } from "./index";

const Container = styled.div`
  display: flex;
  a {
    text-decoration: none;
    color: unset !important;
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  border-top: 1px solid #dfe6e9;
  padding-top: 10px;
  width: 100%;
`;

const PodcastInfo = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  align-items: center;
  width: 300px;
  height: 450px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
  padding: 10px;
`;

const PodcastImage = styled.img`
  margin-top: 10px;
  height: 130px;
  width: 130px;
  border-radius: 5px;
`;

const PodcastTitle = styled.span`
  font-weight: bold;
`;

const PodcastAuthor = styled.span`
  font-style: italic;
  color: #b2bec3;
  margin-top: 5px;
`;

const PodcastDescription = styled.span`
  font-size: 11px;
  font-style: italic;
  margin-top: 5px;
`;

export function Podcast({ getPodcastRequest, podcast, loading, error }) {
  const { id = "1481398762", guid } = useParams();
  const { artworkUrl100, artistName, trackName } = podcast;

  useEffect(() => {
    getPodcastRequest(id);
  }, [getPodcastRequest, id]);

  const findEpisode = (guid = "") => {
    const { episodes = [] } = podcast;

    return episodes.find(e => e.guid === guid);
  };

  const getPodcastDescription = () => {
    try {
      const { podcastsStorage } = localStorage;
      if (podcastsStorage) {
        return JSON.parse(podcastsStorage).find(p => p.id === id).summary;
      }
      return;
    } catch (e) {
      console.log("Error getting podcast description", e);
    }
  };

  return (
    <Container data-testid="podcast">
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <>
          <PodcastInfo data-testid="podcast-info">
            <Link to={`/podcast/${id}`} data-testid="podcast-image">
              <PodcastImage src={artworkUrl100} id="podcast-image" />
            </Link>
            <Column>
              <Link to={`/podcast/${id}`} data-testid="podcast-title">
                <PodcastTitle id="podcast-title">{trackName}</PodcastTitle>
              </Link>
              <PodcastAuthor>by {artistName}</PodcastAuthor>
            </Column>
            <Column>
              Description
              <PodcastDescription data-testid="podcast-description">
                {getPodcastDescription()}
              </PodcastDescription>
            </Column>
          </PodcastInfo>
          <Route
            exact
            path="/podcast/:id"
            render={() => <Episodes episodes={podcast.episodes} />}
          />
          <Route
            exact
            path="/podcast/:id/episode/:guid"
            render={() => <Episode episode={findEpisode(guid)} />}
          />
        </>
      )}
    </Container>
  );
}

const mapStateToProps = ({ podcasts }) => {
  const { podcast, loading, error, podcastList } = podcasts;
  return { podcast, loading, error, podcastList };
};

export default connect(
  mapStateToProps,
  { getPodcastRequest }
)(Podcast);
