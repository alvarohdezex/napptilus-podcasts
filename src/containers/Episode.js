import React from "react";
import styled from "@emotion/styled";

const Container = styled.div`
  width: 100%;
  display: flex;
  text-align: left;
  margin-left: 40px;
  flex-direction: column;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);

  audio::-webkit-media-controls-panel {
    background: #2d3436;
    border-radius: unset !important;
  }
  audio::-webkit-media-controls-timeline,
  audio::-webkit-media-controls-timeline-container,
  audio::-webkit-media-controls-current-time-display,
  audio::-webkit-media-controls-time-remaining-display {
    color: white;
  }
  audio::-webkit-media-controls-mute-button,
  audio::-webkit-media-controls-play-button {
    color: #ffffff;
  }
`;

const Title = styled.h3`
  padding: 10px;
  margin: 0;
`;

const Description = styled.div`
  padding: 10px;
  margin: 0;
`;

const Audio = styled.audio`
  width: 90%;
  height: 30px;
  margin-top: auto;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 15px;
  border-radius: 20px;
`;

function Episode({ episode = {} }) {
  const { title, description, enclosure } = episode;

  return (
    <Container>
      <Title data-testid="episode-title">{title}</Title>
      <Description
        dangerouslySetInnerHTML={{ __html: description }}
        data-testid="episode-description"
      />
      {enclosure && enclosure.link && (
        <Audio controls data-testid="episode-audio">
          <source src={enclosure.link} type={enclosure.type} />
        </Audio>
      )}
    </Container>
  );
}

export default Episode;
