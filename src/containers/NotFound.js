import React from "react";

function NotFound() {
  return <div>Woops, page not found.</div>;
}

export default NotFound;
