import React from "react";
import styled from "@emotion/styled";
import { Link, useParams } from "react-router-dom";
import { fancyTimeFormat } from "../utils/dates";

const Container = styled.div`
  width: 100%;
  display: flex;
  text-align: left;
  margin-left: 40px;
  flex-direction: column;
`;

const EpisodesCount = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  padding-left: 10px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
  font-weight: bold;
  font-size: 18px;
  margin-bottom: 20px;
`;

const EpisodesTable = styled.table`
  padding: 20px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
`;

const EpisodesTableHeader = styled.thead`
  font-weight: bold;
`;

const EpisodesTableBody = styled.tbody`
  tr:nth-of-type(odd) {
    background: #dfe6e9;
  }
`;

const TableRow = styled.tr``;

const TableData = styled.td`
  padding-top: 0.5em;
  padding-bottom: 0.5em;
`;

const EpisodeTitle = styled.span`
  font-weight: bolder;
  color: #0984e3;
`;

function Episodes({ episodes = [] }) {
  const { id } = useParams();
  return (
    <Container>
      <EpisodesCount data-testid="episodes-count">
        Episodes: {episodes.length}
      </EpisodesCount>
      <EpisodesTable>
        <EpisodesTableHeader>
          <TableRow>
            <TableData>Title</TableData>
            <TableData>Date</TableData>
            <TableData>Duration</TableData>
          </TableRow>
        </EpisodesTableHeader>
        <EpisodesTableBody>
          {episodes.map(e => (
            <TableRow key={e.guid} data-testid="episode">
              <TableData>
                <Link to={`/podcast/${id}/episode/${e.guid}`}>
                  <EpisodeTitle data-testid="episode-title">
                    {e.title}
                  </EpisodeTitle>
                </Link>
              </TableData>
              <TableData data-testid="episode-date">
                {new Date(e.pubDate).toLocaleDateString()}
              </TableData>
              <TableData data-testid="episode-duration">
                {fancyTimeFormat(e.enclosure.duration)}
              </TableData>
            </TableRow>
          ))}
        </EpisodesTableBody>
      </EpisodesTable>
    </Container>
  );
}

export default Episodes;
