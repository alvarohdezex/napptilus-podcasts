/* eslint-disable */
describe("<Podcasts />", () => {
  it("Visits the home page", () => {
    cy.visit("http://localhost:3000");
    cy.wait(1500);
  });

  it("Searches for Dolly in the list filter", () => {
    cy.wait(1500);
    cy.get("input").type("dolly");
  });

  it("Clicks on the Dolly's podscast and navigates to it", () => {
    cy.wait(1500);
    cy.get("a")
      .eq(1)
      .click();
  });

  it("On the Dolly's details, navigates to the second episode", () => {
    cy.wait(2500);
    cy.get("a")
      .eq(3)
      .click();
  });

  it("Plays the podcast of Dolly's, named 'Dolly Parton's America Trailer'", () => {
    cy.wait(1500);
    cy.get("audio").click();
  });
});
