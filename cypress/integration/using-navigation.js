/* eslint-disable */
describe("Router navigation", () => {
  it("Visits the home page", () => {
    cy.visit("http://localhost:3000");
    cy.wait(1500);
  });

  it("Searches for Dolly in the list filter", () => {
    cy.wait(1500);
    cy.get("input").type("dolly");
  });

  it("Clicks on the Dolly's podscast and navigates to it", () => {
    cy.wait(1500);
    cy.get("a")
      .eq(1)
      .click();
  });

  it("Comes back to home", () => {
    cy.wait(2500);
    cy.get("a")
      .eq(0)
      .click();
  });

  it("Searches for BobbyCast in the list filter", () => {
    cy.wait(1500);
    cy.get("input").type("BobbyCast");
  });

  it("Clicks on the BobbyCast's podscast and navigates to it", () => {
    cy.wait(1500);
    cy.get("a")
      .eq(1)
      .click();
  });

  it("On the BobbyCast's details, navigates to the second episode", () => {
    cy.wait(2500);
    cy.get("a")
      .eq(3)
      .click();
  });

  it("Comes back to BobbyCast's details clicking the image", () => {
    cy.wait(1500);
    cy.get("#podcast-image").click();
  });

  it("On the BobbyCast's details, navigates to the first episode", () => {
    cy.wait(2500);
    cy.get("a")
      .eq(2)
      .click();
  });

  it("Comes back to BobbyCast's details clicking the title", () => {
    cy.wait(1500);
    cy.get("#podcast-title").click();
  });

  it("Comes back to home", () => {
    cy.wait(2500);
    cy.get("a")
      .eq(0)
      .click();
  });
});
