/* eslint-disable */
describe("Filtering podcasts", () => {
  it("Visits the home page", () => {
    cy.visit("http://localhost:3000");
    cy.wait(1500);
  });

  it("Typing 'Napptilus' in the filtering should display 0 podcasts", () => {
    cy.wait(1500);
    cy.get("input").type("Napptilus");
    cy.wait(1000);
    cy.get("input").clear();
  });

  it("Typing 'Alvaro' in the filtering should display 0 podcasts", () => {
    cy.wait(1500);
    cy.get("input").type("Alvaro");
    cy.wait(500);
  });

  it("Deleting the input value should display every podcast", () => {
    cy.wait(1500);
    cy.get("input").clear();
    cy.wait(500);
  });
});
