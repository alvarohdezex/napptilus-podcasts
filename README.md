## How to run the project

Every script is possible to run with npm (npm run <command>), but I recommend using yarn: [https://yarnpkg.com](https://yarnpkg.com)

## Available Scripts

In the project directory, you can run:

### yarn start - runs the app in development mode

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### yarn start:prod - runs the app in production mode

Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

### yarn test:e2e - launches the e2e tests

In order to test the e2e integration of the app you should run first the app (yarn start), and then run yarn test:e2e

### yarn test - launches the unitary tests

### yarn build - builds the app for production to the `build` folder
